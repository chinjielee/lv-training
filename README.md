# Todo App

# Project XXX

**Goals To Meet: (XXX)**

- [ ] Understand & able to use Gitlab Issue
- [ ] Complete 10/12 [Tutorials](https://www.youtube.com/watch?v=A5S23KS_-bU&list=PLEhEHUEU3x5q-xB1On4CsLPts0-rZ9oos) (Ignore 2 tutorial on firebase)
- [ ] Understand & able to use Vuejs core feature
- [ ] Understand & able to use Laravel core feature
- [ ] Fill in docs of what understood on [Vuejs & Laravel](https://docs.google.com/document/d/XXX) 
- [ ] Understand & able to use Vuejs, Laravel for development

## How to initiate the project (first time only)

1. Open Git bash at any desired folder
1. Git clone to your local machine (bash), open project in vscode
    ```bash 
    git clone https://gitlab.com/XXX.git
    ``` 
1. At VSCode, press `CTRL+SHIFT+P`, type `terminal:selectDefaultShell`, use arrow key to select `Git Bash`, press enter
1. press `CTRL+SHIFT+P` again, type `terminal: Create New Integrated Terminal`, use arrow to select it, press enter
1. in VSCode Integrated Bash terminal, run command `sh init.sh` to install all the required dependencies, until `initialize complete...` appears
    ```bash 
    sh init.sh
    ```
1. by default, the site will be available at  - `localhost:10033`

## How to access MySQL database
1. by default for adminer  - `localhost:100X4`
1. by default for phpmyadmin - `localhost:100X5`

## How to use docker for this project
1. How to down the project
    ```bash 
    docker-compose down 
    ``` 
1. How to up the project again
    ```
    docker-compose up -d 
    ```

## How to use composer or php artisan command for this project
1. Run command `sh shell.sh` to access php docker container
1. once access you may see terminal change into `www-data@xxx:/application$` then u would know you are at the container
1. apply any `php artisan` command or `composer` command at this container environment

<br>
<br>


## Site can be view If traefik is initialized
1. open host file editor add new line
    ~~~
    127.0.0.1 xxx-api.example.test
    127.0.0.1 xxx-adminer.example.test
    127.0.0.1 xxx-phpmyadmin.example.test
    ~~~
1. by default the site will also be available at both 
    - `localhost:100x3` or `xxx-api.example.test`
1. by default for adminer will also be available at both
    - `localhost:100x4` or `xxx-adminer.example.test`
1. by default for phpmyadmin will also be available at both
    - `localhost:100x5` or `xxx-phpmyadmin.example.test`

## Training Version Changes
1.0 > 1.1


1.1 > 1.2
- Added GitFlow, AgileScrum, Git, Teamwork elements.
[Trainee will have a chance to involve in a group discussion on bad UI practises and create Issues together along with trainer(s) to identify and fix the issues. Trainee will also involve in Gitflow, experiencing the first hand of Merge Conflict.]


- Better port allocation
- Added new folder Webpage
- Fixed 1.1 Traefik Issues
- Better Readme Documentation


#!/bin/sh

echo "Setting up environment files (.env) ..."  && \
rm -rf .env && cp -rp .env.example .env && \
echo "docker-compose up ..."  && \
docker-compose up -d && \
echo "initialize complete..."
